/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author user
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
//        Player p1 = new Player("Cesar");
//        Player p2 = new Player("Ulix");
//        
//        Board b = new Board(6, 6);
//        b.insertPiece(0, p1);
//        b.insertPiece(0, p1);
//        b.insertPiece(0, p1);
//        b.insertPiece(0, p1);
//        
//        b.printBoard();
        
        int[][] grid = new int[5][5];
        int width = grid.length;
        int height = grid[0].length;
        int[][] dp = new int[width][height];
        
        Scanner sc = new Scanner(System.in);
        
        HashMap<Integer, Integer> col_count = new HashMap<>();
        
        for (int i = 0; i < width; i++)
            col_count.put(i, 0);
        
        int n;
        
        while(true) {
            n = sc.nextInt();
            
            if (n == 32) break;
            
            int curr_count = col_count.get(n);
            int pos_add = height - curr_count - 1;

            grid[pos_add][n] = 1;
            dp[pos_add][n] = 1;
            
            //Check down
            if (pos_add + 1 < height && pos_add + 1 > 0)
                dp[pos_add][n] += dp[pos_add + 1][n];
            
            //Check left
            if (n - 1 < width && n - 1 > 0)
                dp[pos_add][n] += dp[pos_add][n - 1];
            
            //Check right
            if (n + 1 < width && n + 1 > 0)
                dp[pos_add][n] += dp[pos_add][n + 1];

            //Check upper right
            if ((n + 1 < width && n + 1 > 0) || (pos_add - 1 < height && pos_add - 1 > 0))
                dp[pos_add][n] += dp[pos_add - 1][n + 1];
            
            //Check upper left
            if ((n - 1 < width && n - 1 > 0) || (pos_add - 1 < height && pos_add - 1 > 0))
                dp[pos_add][n] += dp[pos_add - 1][n + 1];
            
            //Check bottom right
            if ((n + 1 < width && n + 1 > 0) || (pos_add - 1 < height && pos_add - 1 > 0))
                dp[pos_add][n] += dp[pos_add - 1][n + 1];
            
            //Check bottom left
            if ((n - 1 < width && n - 1 > 0) || (pos_add + 1 < height && pos_add + 1 > 0))
                dp[pos_add][n] += dp[pos_add + 1][n - 1];
            
            col_count.put(n, curr_count + 1);
            
        }
        
        
        
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                System.out.printf("%d ", grid[i][j]);
            }
            System.out.println("\n");
        }
        
        System.out.println("+++++++++++++++++++++++++++++++++++++++++\n");
        
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {       
                System.out.printf("%d ", dp[i][j]);
            }
            System.out.println("\n");
        }
    }
}
