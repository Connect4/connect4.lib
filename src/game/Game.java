/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.PriorityQueue;

/**
 *
 * @author user
 */
public class Game {
    
    private Board board;
    private PriorityQueue<Player> players;
    
    public Game(Board board) {
        this.board = board;
        players = new PriorityQueue<>(2);
    }
    
    public void addPlayer(Player player) {
        this.players.add(player);
    }
    
    public Board getBoard() {
        return this.board;
    }
    
    public void setBoard(Board board) {
        this.board = board;
    }
    
}
