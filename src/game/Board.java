/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.HashMap;

/**
 *
 * @author Cesar Bretana Gonzalez
 */

public class Board {
    
    private class DynamicData {
        private Directions direction;
        private int value;
        
        public DynamicData() {
            this.value = 1;
            this.direction = null;
        }
        
        public int getValue() {
            return this.value;
        }
        
        public void setValue(int value) {
            this.value = value;
        }
        
        public Directions getDirection() {
            return this.direction;
        }
        
        public void setDirection(Directions direction) {
            this.direction = direction;
        }
        
        public void inc(int deltha) {
            this.value += deltha;
        }
        
        @Override
        public String toString() {
            return String.valueOf(this.value);
        }
    }
    
    private class ResultData {
        private StatusCodes status;
        private Player last_player;
        
        public ResultData(StatusCodes status, Player last_player) {
            this.status = status;
            this.last_player = last_player;
        }
        
        public StatusCodes getStatus() {
            return this.status;
        }
        
        public void setStatus(StatusCodes status) {
            this.status = status;
        }
        
        public Player getLastPlayer() {
            return this.last_player;
        }
        
        public void setLastPlayer(Player last_player) {
            this.last_player = last_player;
        }
    }
        
    private int[][] grid;
    private DynamicData[][] dp;
    private int width;
    private int height;
    private HashMap<Integer, Integer> col_count;
    
    
    public Board(int width, int height) {
        this.grid = new int [width][height];
        this.dp = new DynamicData[width][height];
        this.width = width;
        this.height = height;
        col_count = new HashMap<>();
        
        for (int i = 0; i < width; i++)
            col_count.put(i, 0);
        
    }
    
    public int get(int i, int j) {
        return this.grid[i][j];
    }
    // Must return a ResultData
    // to control the 'response' in the addPiece method
    public void set(int i, int j, Player player) {
        int value = player.getId();
        
        this.grid[i][j] = value;
        this.dp[i][j] = new DynamicData();
        
        // Checking left or right
        if (getLeft(i, j) == value) {
            dp[i][j].inc(dp[i - 1][j].getValue());
            dp[i][j].setDirection(Directions.Horizontal);
        }
        if (getRight(i, j) == value) {
            dp[i][j].inc(dp[i + 1][j].getValue());
            dp[i][j].setDirection(Directions.Horizontal);
        }
        // Checking down (don't need to check up, because I will never 
        // have a piece on top of another without checking it.
        if (getDown(i, j) == value) {
            dp[i][j].inc(dp[i + 1][j].getValue());
            dp[i][j].setDirection(Directions.Vertical);
        }
        
        // Now to diagonals
        if (getUpperLeft(i, j) == value) {
            dp[i][j].inc(dp[i - 1][j + 1].getValue());
            dp[i][j].setDirection(Directions.Diagonal);
        }
        
        if (getUpperRight(i, j) == value) {
            dp[i][j].inc(dp[i + 1][j + 1].getValue());
            dp[i][j].setDirection(Directions.Diagonal);
        }
        
        if (getBottomLeft(i, j) == value) {
            dp[i][j].inc(dp[i - 1][j - 1].getValue());
            dp[i][j].setDirection(Directions.Diagonal);
        }
        
        if (getBottomRight(i, j) == value) {
            dp[i][j].inc(dp[i + 1][j - 1].getValue());
            dp[i][j].setDirection(Directions.Diagonal);
        }
    }
    
    private int getLeft(int x, int y) {
        return (x - 1 < 0 || x - 1 > this.grid.length) ? -1 : this.grid[x - 1][y];        
    }
    
    private int getRight(int x, int y) {
        return (x + 1 < 0 || x + 1 > this.grid.length - 1) ? -1 : this.grid[x + 1][y];
    }
    
    
    private int getDown(int x, int y) {
        return (x + 1 > 0 || x + 1 <= height) ? this.grid[x + 1][y] : -1;
    }
    
    private int getUpperLeft(int x, int y) {
        return (x - 1 < 0 || x - 1 > this.grid.length - 1) || (y + 1 < 0 || y + 1 > this.grid[0].length - 1) ? -1 : this.grid[x - 1][y + 1];
    }
    
    private int getUpperRight(int x, int y) {
        return (x + 1 < 0 || x + 1 > this.grid.length - 1) || (y + 1 < 0 || y + 1 > this.grid[0].length - 1) ? -1 : this.grid[x + 1][y + 1];
    }
    
    private int getBottomLeft(int x, int y) {
        return (x - 1 < 0 || x - 1 > this.grid.length - 1) || (y - 1 < 0 || y - 1 > this.grid[0].length - 1) ? -1 : this.grid[x - 1][y - 1];
    }
    
    private int getBottomRight(int x, int y) {
        return (x + 1 < 0 || x + 1 > this.grid.length - 1) || (y - 1 < 0 || y - 1 > this.grid[0].length - 1) ? -1 : this.grid[x + 1][y - 1];
    }
    
    
    
    public ResultData insertPiece(int pos, Player player) {
        
        ResultData result;
        
        int curr_len = grid.length;
        int[] arr = new int[height];
        
        
        
        int curr_count = this.col_count.get(pos);
        
        if (is_filled(grid[pos]))
            result = new ResultData(StatusCodes.FULL_COLUMN, player);
        else {
            this.set((curr_len - curr_count) - 1, pos, player);
            this.col_count.put(pos, curr_count + 1);
            
            result = new ResultData(StatusCodes.OK, player);
        }
        
        return result;
    }
    
    public void printBoard() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                System.out.printf("%d ", grid[i][j]);
            }
            System.out.println("\n");
        }
    }
    
    
    private boolean is_filled(int [] array) {
        return false;
    }
    
//    private int count(int[] array) {
//        int res = 0;
//        for (int i = 0; i < array.length; i++) {
//            if(array[i] != 0) 
//                res++;
//        }
//        return res;
//    }    
}
