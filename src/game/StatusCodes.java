/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 *
 * @author user
 */
public enum StatusCodes {
    OK,
    FULL_COLUMN,
    WON_VERTICAL,
    WON_HORIZONTAL,
    WON_DIAGONAL,
    DRAW
}
