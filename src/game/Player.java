/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.Random;

/**
 *
 * @author Cesar Bretana Gonzalez
 */
public class Player {
    private int id;
    private String name;
    
    public Player(String name) {
        
        Random r = new Random();
        
        this.id = r.nextInt(100) + 1;
        this.name = name;
    }
    
    public int getId() {
        return this.id;
    }
       
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
}